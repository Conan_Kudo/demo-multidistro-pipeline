# Demo multi-distro image pipeline

This project is a simple sample showing a pipeline for simultaneously producing images for CentOS Stream 9 and Ubuntu 22.04.

The idea here is to show how it is possible to use common tooling to support multiple distributions at once.

The parameters of how it is done is constrained to a very simple case so that it can run in GitLab CI. More complex requirements would require a more complex harness.

# Licensing

The kiwi descriptions are derived from [Datto's demo golden images from DevConf.US 2022](https://github.com/datto/devconfus22-demo-golden-image-descriptions),
which are licensed under the Apache Software License, version 2.0. See `LICENSE-ASLv2` for details.

The GitLab CI scripts are derived partly from [Datto's Spacewalk Debian client packaging project](https://gitlab.com/datto/engineering/spacewalk-debian-client-packages),
which are licensed under the GNU General Public License, version 2.0 (or at your option, any later version). See `LICENSE-GPLv2` for details.

The packaging spec files are works licensed under the GNU General Public License, version 2.0 (or at your option, any later version). See `LICENSE-GPLv2` for details.
