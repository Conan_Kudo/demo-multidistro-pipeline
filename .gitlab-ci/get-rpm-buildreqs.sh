#!/bin/bash
# Script to get rpm build-time requirements and install them
# Author: Neal Gompa <neal@gompa.dev>
# Fedora-License-Identifier: GPLv2+
# SPDX-2.0-License-Identifier: GPL-2.0+
# SPDX-3.0-License-Identifier: GPL-2.0-or-later

# Copyright (C) 2023  Neal Gompa.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


set -eu -o pipefail

PACKAGES="hello pello"

BUILDREQS=""

if [ ! -x /usr/bin/rpmspec ]; then
	dnf -y install /usr/bin/rpmspec
fi

for PKG in ${PACKAGES[@]}
do
	NEW_BUILDREQS=$(rpmspec -q --srpm --buildrequires packages/${PKG}/${PKG}.spec | xargs)
	BUILDREQS="${NEW_BUILDREQS} ${BUILDREQS}"
done

dnf -y install $BUILDREQS
