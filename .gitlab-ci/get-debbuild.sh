#!/bin/bash
# Script to install and setup debbuild
# Author: Neal Gompa <neal@gompa.dev>
# Fedora-License-Identifier: GPLv2+
# SPDX-2.0-License-Identifier: GPL-2.0+
# SPDX-3.0-License-Identifier: GPL-2.0-or-later

# Copyright (C) 2018  Datto, Inc.
# Copyright (C) 2023  Neal Gompa.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


set -eu -o pipefail

# Download and install dependencies
DEBIAN_FRONTEND=noninteractive apt update
DEBIAN_FRONTEND=noninteractive apt -y install gnupg2 curl

# Download and install debbuild
echo 'deb http://download.opensuse.org/repositories/Ubuntu:/debbuild/Ubuntu_22.04/ /' | tee /etc/apt/sources.list.d/Ubuntu_debbuild.list
curl -fsSL https://download.opensuse.org/repositories/Ubuntu:debbuild/Ubuntu_22.04/Release.key | gpg --dearmor | tee /etc/apt/trusted.gpg.d/Ubuntu_debbuild.gpg > /dev/null
DEBIAN_FRONTEND=noninteractive apt update
DEBIAN_FRONTEND=noninteractive apt -y install debbuild

# Configure debbuild behavior for CI with ~/.debmacros
echo "%_default_verbosity 2" >> ~/.debmacros
echo "%_topdir %(pwd)" >> ~/.debmacros
