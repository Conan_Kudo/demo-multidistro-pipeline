#!/bin/bash
# Script to build the packages
# Author: Neal Gompa <neal@gompa.dev>
# Fedora-License-Identifier: GPLv2+
# SPDX-2.0-License-Identifier: GPL-2.0+
# SPDX-3.0-License-Identifier: GPL-2.0-or-later

# Copyright (C) 2018  Datto, Inc.
# Copyright (C) 2023  Neal Gompa.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


set -eu -o pipefail

PACKAGES="hello pello"

for PKG in ${PACKAGES[@]}
do
	pushd packages/$PKG
	mkdir -p {BUILD,BUILDROOT,RPMS,SRPMS,SPECS,SOURCES}
	cp -v *.* SOURCES
	cp -v *.spec SPECS
	rpmbuild -ba ${PKG}.spec
	popd
done
