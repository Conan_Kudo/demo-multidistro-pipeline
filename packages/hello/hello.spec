Name:            hello
Version:         2.10
Release:         1%{?dist}
Summary:         A Hello World application from the GNU Project

License:         GPL-3.0-or-later
URL:             https://www.gnu.org/software/hello/
Source0:         http://ftp.gnu.org/gnu/hello/%{name}-%{version}.tar.gz

%if "%{_vendor}" == "debbuild"
Group:           misc
Packager:        Neal Gompa <neal@gompa.dev>
Requires(post):  info
Requires(preun): info
%endif

BuildRequires:   gcc
BuildRequires:   make
BuildRequires:   gettext


%description
The GNU Hello program is a Free Software take on
the classical Hello World application. It uses autotools
and offers extensive language support. It is often used
as an example of how Free Software can be written and packaged.

%prep
%autosetup

%build
%configure
%make_build

%install
%make_install
rm -fv %{buildroot}%{_infodir}/dir

%find_lang %{name}

%if "%{_vendor}" == "debbuild"
%post
/sbin/install-info %{_infodir}/%{name}.info %{_infodir}/dir || :

%preun
# Run only on final removal
if [ $1 = "remove" || $1 = "purge" ]; then
/sbin/install-info --delete %{_infodir}/%{name}.info %{_infodir}/dir || :
fi
%endif

%files -f %{name}.lang
%{_bindir}/%{name}
%{_infodir}/%{name}.info*
%{_mandir}/man1/%{name}.1*
%doc README NEWS ChangeLog AUTHORS TODO THANKS
%license COPYING


%changelog
* Fri Jul 28 2023 Neal Gompa <neal@gompa.dev> - 2.10-1
- Initial package.
