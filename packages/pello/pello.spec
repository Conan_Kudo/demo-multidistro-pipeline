# Created by pyp2rpm-3.3.8
%global pypi_name pello
%global pypi_version 1.0.4

Name:           python-%{pypi_name}
Version:        %{pypi_version}
Release:        1%{?dist}
Summary:        An example Python Hello World package

%if "%{_vendor}" == "debbuild"
# Maps to required "Maintainer" field
Packager:       Neal Gompa <neal@gompa.dev>
# Maps to required "Section" field
Group:          misc
%endif

License:        MIT-0
URL:            https://github.com/fedora-python/Pello
Source0:        https://files.pythonhosted.org/packages/source/p/%{pypi_name}/Pello-%{pypi_version}.tar.gz
BuildArch:      noarch

%if "%{_vendor}" == "debbuild"
BuildRequires:  python3-dev
BuildRequires:  python3-deb-macros
BuildRequires:  python3-blessings
BuildRequires:  python3-setuptools
%else
BuildRequires:  python3-devel
BuildRequires:  python3dist(blessings)
BuildRequires:  python3dist(setuptools)
%endif

%description
Pello is an example package that will be used as a part of Fedora Python
Packaging Guidelines. The only thing that this package does is printing Hello
World! on the command line.

%package -n     python3-%{pypi_name}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{pypi_name}}
%if "%{_vendor}" == "debbuild"
Requires:       python3-blessings
Requires:       python3-setuptools
%endif
# Allow it to be installed using "pello" name
Provides:       %{pypi_name} = %{version}-%{release}

%description -n python3-%{pypi_name}
Pello is an example package that will be used as a part of Fedora Python
Packaging Guidelines. The only thing that this package does is printing Hello
World! on the command line.

%prep
%autosetup -n Pello-%{pypi_version}

%build
%py3_build

%install
%py3_install

%check
%{__python3} setup.py test

%files -n python3-%{pypi_name}
%license LICENSE.txt
%doc README.md
%{_bindir}/pello_greeting
%{python3_sitelib}/%{pypi_name}
%{python3_sitelib}/Pello-%{pypi_version}-*

%changelog
* Fri Jul 28 2023 Neal Gompa <neal@gompa.dev> - 1.0.4-1
- Initial package.
